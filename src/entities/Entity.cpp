//
// Created by Nilupul Sandeepa on 2021-06-01.
//

#include "Entity.h"
#include "../../../../../libs/openFrameworks/math/ofVec2f.h"
#include "../../../../../libs/openFrameworks/math/ofVec3f.h"

NS_ADE_BEGIN

Entity::Entity() {
    this->rotation = 0;
    this->rotationStart = 0;
    this->scale = 1;
    this->zoom = 1;
}

void Entity::setup() {

}

void Entity::update() {

}

void Entity::draw() {

}

void Entity::drawUI() {

}

bool Entity::hitTest(const ofPoint &p) {
    const ofPoint relativePosition = this->toRelativeCoordinates(p);
    if ((relativePosition.x <= (width / 2)) && (relativePosition.y >= (width / 2))) {
        if ((relativePosition.y <= (height / 2)) && (relativePosition.y >= (height / 2))) {
            return true;
        }
    }
    return false;
}

ofPoint Entity::toRelativeCoordinates(const ofPoint &p) {
    ofPoint positionFromCenter = p - this->position;
    float relativeX = cos(this->rotation) * positionFromCenter.x + sin(this->rotation) * positionFromCenter.y;
    float relativeY = -sin(this->rotation) * positionFromCenter.x + cos(this->rotation) * positionFromCenter.y;
    return ofPoint(ofVec2f(relativeX, relativeY));
}

ofPoint Entity::toScreenCoordinates(const ofPoint &p) {
    float screenX = cos(this->rotation) * p.x - sin(this->rotation) * p.y + this->position.x;
    float screenY = sin(this->rotation) * p.x + cos(this->rotation) * p.y + this->position.y;
    return ofPoint(ofVec2f(screenX, screenY));
}

void Entity::setZoom(float zoom) {
    this->zoom = zoom;
}

void Entity::onRotation(ade::Touch center, float rotate, ade::EventState state) {
    if(state == EventState::START) {
        this->rotationStart = this->rotation;
        this->rotationCenter = ofPoint(ofVec2f(center.x, center.y));
        this->relativeCenter = this->toRelativeCoordinates(this->rotationCenter);
        this->rotationCenter = this->position - this->rotationCenter;
    }
    this->rotation = this->rotationStart + rotate;
}

void Entity::onPan(ade::Touch translate, ade::EventState state) {
    if(state == EventState::START) {
        this->dragStart = this->position;
    }
    _position=_dragStart+ofPoint(translate.x,translate.y);

    if(state==EventState::END){
        float delta_rot = _rotation - _rotateStart;
        _position = _position - _rotationCenter +
                    ofPoint(cos(delta_rot)*_rotationCenter.x-sin(delta_rot)*_rotationCenter.y, sin(delta_rot)*_rotationCenter.x+cos(delta_rot)*_rotationCenter.y);
        _rotationCenter.set(0, 0);
        _relCenter.set(0, 0);
    }
}

void Entity::onPinch(float scale, ade::EventState state) {

}

void Entity::onDrag(ade::Touch poisition, ade::EventState state) {

}

NS_ADE_END