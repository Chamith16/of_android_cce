//
// Created by Nilupul Sandeepa on 2021-05-29.
//

#ifndef BLENDUTILS_H
#define BLENDUTILS_H

#include "Macros.h"

#include "../../../../../libs/openFrameworks/utils/ofLog.h"
#include "../../../../../libs/openFrameworks/gl/ofFbo.h"
#include "../../../../../libs/openFrameworks/graphics/ofGraphics.h"

NS_ADE_BEGIN

class BlendUtils {
    public:
        static void clearBuffer(ofFbo& frameBuffer);
};

NS_ADE_END

#endif //BLENDUTILS_H