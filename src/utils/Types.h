//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#ifndef TYPES_H
#define TYPES_H

#include "Macros.h"

NS_ADE_BEGIN

enum class EventState {
    READY,
    START,
    UPDATE,
    END,
    CANCEL
};

enum class CanvasMode {
    BRUSH,
    FILL_BUCKET,
    COLOR_PICKER,
    MAGIC_BRUSH
};

enum class BlendMode {
    NORMAL,
    ADDITIVE,
    ERASE,
    MULTIPLY,
    COLORBURN,
    SCREEN,
    DIFFERENCE,
    EXCLUSION
};

enum class BrushType {
    PEN,
    INK_BRUSH,
    HIGHLIGHTER,
    WATERCOLOR, // 3
    ERASER,
    PENCIL,
    ANGLED_HIGHLIGHTER,
    SPRAY_BRUSH, // 7
    NEON_BRUSH,
    FILL_BRUSH,
    FILL_PATTERN_BRUSH,
    PATTERN_BRUSH,
    WOW_BRUSH, // 12
    THREE_DIMEN_BRUSH,
    STAMP_BRUSH,
    PROGRESSIVE_BRUSH,
    FLORAL_BRUSH,
    CHALK, // 17
    PLATIGNUM,
    FILTER_BRUSH,
    KIDS_WATERCOLOR,
    KIDS_PENCIL,
    GRADIENT_BRUSH,
    TILE_BRUSH,
    CUTTER
};


NS_ADE_END

#endif //TYPES_H