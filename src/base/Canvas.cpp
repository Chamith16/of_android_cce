//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#include "Canvas.h"

NS_ADE_BEGIN

Canvas::Canvas(int width, int height) {
    this->width = ofNextPow2(width);
    this->height = ofNextPow2(height);
    this->visibleWidth = width;
    this->visibleHeight = height;

    this->canvasChangeCallback = NULL;
    this->colorPickerCallback = NULL;

    this->canvasMode = CanvasMode::BRUSH;
    this->previousCanvasMode = CanvasMode::BRUSH;

    this->shouldLoadPaintings = true;
    this->shouldShowBackground = false;
    this->shouldShowOverlay = false;
    this->needLayerReloading = false;
    this->isColorPickerEnabled = false;
    this->isParticlesEnabled = false;
    this->isPreUndoStepAdded = false;
    this->isIpadProHack = (width == 2732);
    this->isFrameEnabled = false;
    this->isMagicEnabled = false;
    this->changesStarted = 0;


    //Undo
    //Save utils

    this->background = NULL;
    this->numberOfSimultaneousGestures = 0;
    this->camera = new Camera(this->visibleWidth, this->visibleHeight);

    //this.
}

void Canvas::setup() {
    ofFboSettings fboSettings;
    fboSettings.width = this->width;
    fboSettings.height = this->height;

    this->sharedBuffer.allocate(fboSettings);

    //Clear framebuffer
    BlendUtils::clearBuffer(this->sharedBuffer);

    //Set shared buffer to other components

    //Tool setting up

    //Live particles
}

void Canvas::draw() {
    ofClear(255, 255, 0, 255);
    ofSetColor(255, 0, 0, 255);
    ofDrawRectangle(0, 0, 0, 1080, 1080);
}

void Canvas::setCanvasCallback(ChangeCallback callback) {
    this->canvasChangeCallback = callback;
    this->canvasChangeCallback(EventState::READY);
}

NS_ADE_END