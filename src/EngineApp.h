#pragma once

#include "ofMain.h"
#include "ofxAndroid.h"
#include "../../../../addons/ofxAndroid/src/ofxAndroidApp.h"
#include "utils/Macros.h"
#include "base/Canvas.h"

NS_ADE_BEGIN

class EngineApp : public ofxAndroidApp{
	
	public:
		EngineApp();

		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void windowResized(int w, int h);

		void touchDown(int x, int y, int id);
		void touchMoved(int x, int y, int id);
		void touchUp(int x, int y, int id);
		void touchDoubleTap(int x, int y, int id);
		void touchCancelled(int x, int y, int id);
		void swipe(ofxAndroidSwipeDir swipeDir, int id);

		void pause();
		void stop();
		void resume();
		void reloadTextures();

		bool backPressed();
		void okPressed();
		void cancelPressed();

		//Getters
		Canvas* getCanvas();

		void setupCanvas(int width, int height);
		void setCanvasCallback(ChangeCallback callback);

	protected:
		Canvas* canvas;
};

NS_ADE_END